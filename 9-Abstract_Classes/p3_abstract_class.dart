abstract class Parent {
  void property() {
    print("Gold, Bunglow, flats, cars");
  }

  void career();
  void marry();
}

class Child extends Parent {
  void career() {
    print("Coder");
  }

  void marry() {
    print("Bhumi");
  }
}

void main() {
  Child obj = new Child();

  obj.property();
  obj.career();
  obj.marry();
}
