abstract class Demo {
  Demo() {
    print("In Demo Constructor");
  }

  void fun1() {
    print("In fun-1");
  }

  void fun2();
}

class DemoChild extends Demo {
  DemoChild() {
    print("In DemoChild Constructor");
  }

  void fun2() {
    print("In fun-2");
  }
}

void main() {
  DemoChild obj = new DemoChild();
  obj.fun1();
  obj.fun2();
}
