abstract class Developer {
  void develop() {
    print("We Build Softwares");
  }

  void devType();
}

class MobileDev extends Developer {
  void devType() {
    print("Flutter Developer");
  }
}

class WebDev extends Developer {
  void devType() {
    print("FrontEnd Developer");
  }
}

void main() {
  Developer obj1 = new MobileDev();
  obj1.develop();
  obj1.devType();

  Developer obj2 = new WebDev();
  obj2.develop();
  obj2.devType();

  WebDev obj3 = new WebDev();
  obj3.develop();
  obj3.devType();

/*
  Developer obj4 = new Developer();
  //Error: The class 'Developer' is abstract and can't be instantiated.
  obj4.develop();
  obj4.devType();
*/
}
