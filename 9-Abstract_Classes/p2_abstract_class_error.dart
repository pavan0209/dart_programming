abstract class Parent {
  void property() {
    print("Gold, Bunglow, flats, cars");
  }

  void career();
  void marry();
}

void main() {
  Parent obj = new Parent();
  //Error: The class 'Parent' is abstract and can't be instantiated
  obj.property();
}
