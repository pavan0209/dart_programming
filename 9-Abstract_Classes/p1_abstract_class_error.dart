class Parent {
  void property() {
    print("Gold, Bunglow, flats, cars");
  }

  void career();
  void marry();
}

class Child extends Parent {}

void main() {}
