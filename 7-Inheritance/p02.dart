class Parent {
  int x = 10;
  String str = "Name";

  void parentMethod() {
    print("In parent display method");
  }
}

class Child extends Parent {
  int y = 20;
  String str2 = "Data";

  void childMethod() {
    print(y);
    print(str2);
  }
}

void main() {
  Parent obj = new Parent();
  /*
  print(obj.y);
  print(obj.str2);
  obj.childMethod();
  */

  print(obj.x);
  print(obj.str);
  obj.parentMethod();
}
