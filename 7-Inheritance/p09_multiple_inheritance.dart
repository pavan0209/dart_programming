class ICC {
  ICC() {
    print("ICC Constructor");
  }
}

class BCCI {
  BCCI() {
    print("BCCI Constructor");
  }
}

class IPL extends BCCI, ICC {
    // Error: Each class definition can have at most one extends clause.
    // Dart doen't support multiple inheritance through classes
  IPL() {
    print("IPL Constructor");
  }
}

void main() {
  IPL obj = new IPL();
}
