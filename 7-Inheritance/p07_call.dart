class Parent {
  Parent() {
    print("Parent Constructor");
  }

  call() {
    print("In Method Call");
  }
}

class Child extends Parent {
  Child() {
    super(); // calls the call() method
    print("Child Constructor");
  }
}

void main() {
  Child obj = new Child();
  obj();
}
