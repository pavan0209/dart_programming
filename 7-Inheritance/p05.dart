class Parent {
  int x = 10;
  String str1 = "Name";

  Parent() {
    print("Parent");
  }

  void parentMethod() {
    print(x);
    print(str1);
  }
}

class Child extends Parent {
  int x = 20;
  String str1 = "data";

  Child() {
    print("Child");
  }

  void childMethod() {
    print(x);
    print(str1);
  }
}

void main() {
  Parent obj1 = new Parent();
  Child obj2 = new Child();

  print(obj1.x);
  print(obj1.str1);
  obj1.parentMethod();

  print(obj2.x);
  print(obj2.str1);
  obj2.childMethod();
}
