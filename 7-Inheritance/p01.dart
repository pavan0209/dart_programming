class Parent {
  int x = 10;
  String str = "SurName";

  void parentDisp() {
    print("In parent display method");
  }
}

class Child extends Parent {}

void main() {
  Child obj = new Child();

  print(obj.x);
  print(obj.str);

  obj.parentDisp();
}
