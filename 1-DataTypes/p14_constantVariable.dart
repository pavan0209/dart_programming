
void main() {

  const int x = 10;
  const int y = 30;
  const int z;      // Error: The const variable 'z' must be initialized. 
                    // Try adding an initializer ('= expression') to the declaration.

  x = 50;           // Error: Can't assign to the const variable 'x'
}