/*
  Dart doesn't support method overloading because in dart method is also an object and there can not be more
  than one same object in same scope
*/

class Demo {
  void disp(int) {}

  void disp(String str, int y) {
    //Error: 'disp' is already declared in this scope.
  }
}
