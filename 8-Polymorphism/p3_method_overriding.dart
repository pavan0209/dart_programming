class Parent {
  void career() {
    print("Engineering");
  }

  void marry() {
    print("Deepika");
  }
}

class Child extends Parent {
  void marry() {
    print("Disha");
  }
}

void main() {
  Child obj = new Child();

  obj.career();
  obj.marry();

  Parent obj2 = new Parent();

  obj2.career();
  obj2.marry();
}
