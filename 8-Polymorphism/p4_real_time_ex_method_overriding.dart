class Amazon {
  int? foundYear;
  String? ownerName;

  Amazon(this.foundYear, this.ownerName);

  void compInfo() {
    print(foundYear);
    print(ownerName);
  }

  void server() {
    print("AWS");
  }

  void functionality() {
    print("Online Shopping / E-Commerce");
  }
}

class AmazonPrimeVideo extends Amazon {
  int? foundYear;

  AmazonPrimeVideo(this.foundYear, int foundationYear, String ownerName)
      : super(foundYear, ownerName);

  void functionality() {
    print("Video Streaming / Movies");
  }
}

void main() {
  Amazon obj = new AmazonPrimeVideo(2004, 1999, "Jeff Bezos");

  obj.server();
  obj.compInfo();
  obj.functionality();
}
