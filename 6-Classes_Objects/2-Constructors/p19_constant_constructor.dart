class Player {
  /*
  int? jerNo;
  String? pName;

  const Player(this.jerNo, this.pName);
  Error: Constructor is marked 'const' so all fields must be final.
  */
  final int? jerNo;
  final String? pName;

  const Player(this.jerNo, this.pName);
}

void main() {}
