class Demo {
  int x = 10;


  void Demo() {
      //Error: Constructors can't have a return type.
      //Try removing the return type.
    print("Constructor");
  }
}

void main() {
  Demo obj = new Demo();
}
