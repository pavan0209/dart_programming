class Employee {
  int? empId;
  String? empName;

  Employee() {
    print("Default Constructor");
  }

  Employee.constructor(int empId, String empName) {
    print("Named Constructor");
  }
}

void main() {
  Employee obj1 = new Employee();

  Employee obj2 = new Employee.constructor(100, "Pavan");
}
