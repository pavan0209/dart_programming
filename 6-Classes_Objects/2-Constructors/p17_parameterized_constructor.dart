class Employee {
  int? empId;
  String? empName;

  Employee(this.empId, this.empName) {
    print("Parameterized Constructor");
  }

  void printData() {
    print(empId);
    print(empName);
  }
}

void main() {
  Employee obj = new Employee(100, "Pavan");
  obj.printData();
}
