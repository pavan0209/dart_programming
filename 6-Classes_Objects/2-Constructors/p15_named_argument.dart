class Company {
  int? empCount;
  String? compName;

  Company({this.empCount, this.compName = "Microsoft"});

  void compInfo() {
    print(empCount);
    print(compName);
  }
}

void main() {
  Company obj1 = new Company(empCount: 1000, compName: "Veritas");
  Company obj2 = new Company(compName: "Pubmatic", empCount: 2000);

  obj1.compInfo();
  obj2.compInfo();
}
