class Company {
  int? empCount;
  String? compName;

  Company(this.empCount, {this.compName = "Microsoft"});

  void compInfo() {
    print(empCount);
    print(compName);
  }
}

void main() {
  Company obj1 = new Company(1000);
  //Company obj2 = new Company(2000, "Pubmatic");
  //Error: Too many positional arguments: 1 allowed, but 2 found. Try removing the extra positional arguments.

  obj1.compInfo();
}
