import 'p5_setter_way3.dart';

void main() {
  Demo obj = new Demo(10, "Kanha", 1.5);

  obj.disp();

  obj.setX = 15;
  obj.setName = "Pavan";
  obj.setSal = 2.0;

  obj.disp();
}
