class Player {
  int jerNo = 7;
  String name = "MS Dhoni";

  void playerInfo() {
    print(jerNo);
    print(name);
  }
}

void main() {
  Player obj = new Player();

  obj.playerInfo();

  obj.jerNo = 45;
  obj.name = "Rohit Sharma";

  obj.playerInfo();
}
