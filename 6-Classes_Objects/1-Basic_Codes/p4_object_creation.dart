/*
  Different Ways to create object in dart
*/

class Company {
  int empCount = 500;
  String compName = "Google";
  String location = "Pune";

  void compInfo() {
    print(empCount);
    print(compName);
    print(location);
  }
}

void main() {
  Company obj1 = new Company();
  obj1.compInfo();

  Company obj2 = Company();
  obj2.compInfo();

  new Company().compInfo();

  Company().compInfo();
}
