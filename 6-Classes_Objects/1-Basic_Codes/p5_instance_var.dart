class Employee {
  int empId = 10;
  String empName = "Vinit";
  double empSal = 11.05;

  void compInfo() {
    print(empId);
    print(empName);
    print(empSal);
  }
}

void main() {
  Employee obj1 = new Employee();
  obj1.compInfo();

  Employee obj2 = new Employee();
  obj2.compInfo();

  print("------------------------");

  obj1.empSal = 20.05;

  obj1.compInfo();
  obj2.compInfo();
}
