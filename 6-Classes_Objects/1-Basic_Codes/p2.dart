import 'dart:io';

class Employee {
  int? empId = 1;
  String? empName = "Pavan";
  double? empSal = 2.0;

  void getData() {
    print("Id :: $empId");
    print("Name :: $empName");
    print("Salary :: $empSal");
  }
}

void main() {
  Employee emp = new Employee();
  emp.getData();

  stdout.write("\nEnter new empId :: ");
  emp.empId = int.parse(stdin.readLineSync()!);

  stdout.write("Enter name :: ");
  emp.empName = stdin.readLineSync();

  stdout.write("Enter salary :: ");
  emp.empSal = double.parse(stdin.readLineSync()!);

  stdout.writeln();
  emp.getData();
}
