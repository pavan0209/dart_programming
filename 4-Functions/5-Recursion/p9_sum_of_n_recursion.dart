int fun(int x) {
  if (x < 1) return x;

  return x + fun(x - 1);
}

void main() {
  print(fun(5));
}
