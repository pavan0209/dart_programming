int fun(int x) {
  int sum = 0;

  for (int i = 1; i <= x; i++) {
    sum += i;
  }

  return sum;
}

void main() {
  print(fun(5));
}
