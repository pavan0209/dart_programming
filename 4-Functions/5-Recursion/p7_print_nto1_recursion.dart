void fun(int x) {
  if (x < 1) return;

  print(x);
  fun(x - 1);
}

void main() {
  fun(5);
}
