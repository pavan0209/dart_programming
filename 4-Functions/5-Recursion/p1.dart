void fun() {
  print("In fun");
  fun();
}

void main() {
  fun();
}

/*
op :
  In fun 
  ...
  ...
  ...
  Stack OverFlow
*/