//Arrow Function

int add(a, b) => a + b;
int sub(a, b) => a - b;
int mul(a, b) => a * b;
num div(a, b) => a / b;

void main() {
  int x = 10;
  int y = 20;

  print(add(x, y));
  print(sub(x, y));
  print(mul(x, y));
  print(div(x, y));
}
