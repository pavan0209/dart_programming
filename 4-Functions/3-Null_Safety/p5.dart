
void playerInfo({int? jerNo, String? name}, [String team = "India"]) {  //Error: Expected ')' before this.

  print(jerNo);
  print(name);
  print(team);  //Error: Undefined name 'team'.
}

void main () {

  playerInfo(name : "virat", jerNo: 18);
  playerInfo(name : "rohit");
  playerInfo(name : null, jerNo : null);
}