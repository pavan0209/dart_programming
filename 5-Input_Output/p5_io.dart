import 'dart:io';
import 'dart:core';

void main() {
  stdout.write("Enter employee id :: ");
  int? empId = int.parse(stdin.readLineSync()!);

  stdout.write("Enter employee name :: ");
  String? empName = stdin.readLineSync();

  stdout.write("Enter employee salary :: ");
  double? empSal = double.parse(stdin.readLineSync()!);

  stdout.write("Id : $empId  ");
  stdout.write("Name : $empName  ");
  stdout.write("Salary : $empSal");

  stdout.writeln();

  stdout.writeln("Id : $empId , Name : $empName , Salary : $empSal");
}
