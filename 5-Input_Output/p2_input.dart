import 'dart:io';

void main() {
  //String name = stdin.readLineSync();
  //Error: A value of type 'String?' can't be assigned to a variable of type 'String' because
  //'String?' is nullable and 'String' isn't.

  String? name = stdin.readLineSync();
  print("Name = $name");
}
